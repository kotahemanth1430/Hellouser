import java.util.*;
public class Java1{
   public static void main(String[] args){
       Scanner in=new Scanner(System.in);
       System.out.println("Enter user name: ");
       String name=in.nextLine();
       boolean c= isString(name);
       if(c){
           System.out.println("Hello " +name);
       }
   }
   static boolean isString(String n)
   {
       int i;
       for(i=0;i<n.length();i++){
           if(n.charAt(i)<48 || n.charAt(i)>57){
               return true;
           }
       }
       return false;
   }
}
